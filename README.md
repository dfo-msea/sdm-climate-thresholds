# Exploration of whether environmental conditions exceed thresholds for Pacific Cod

__Main author:__  Amber Holdsworth and Patrick Thompson  
__Contributors:__ 
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis 
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: amber.holdsworth@dfo-mpo.gc.ca and patrick.thompson@dfo-mpo.gc.ca


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
- [Requirements](#requirements)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective


## Summary

## Status
Ongoing

## Contents
R code for loading data and performing analysis. 

## Methods

## Requirements

## Caveats

